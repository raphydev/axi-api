package com.axopen.axogeneratorapi.service;

import com.axopen.axogeneratorapi.domain.Axomot;
import com.axopen.axogeneratorapi.repository.AxomotRepository;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AxomotService {

    private final AxomotRepository axomotRepository;

    public AxomotService(AxomotRepository axomotRepository) {
        this.axomotRepository = axomotRepository;
    }

    public Axomot addAxomot(Axomot axomot) {
        return axomotRepository.save(axomot);
    }

    public List<Axomot> getAllAxomots() {
        return axomotRepository.findAll();
    }
}
