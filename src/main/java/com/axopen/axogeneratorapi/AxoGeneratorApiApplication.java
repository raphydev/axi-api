package com.axopen.axogeneratorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AxoGeneratorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AxoGeneratorApiApplication.class, args);
	}

}
