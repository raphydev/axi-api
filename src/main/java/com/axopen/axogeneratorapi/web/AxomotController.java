package com.axopen.axogeneratorapi.web;

import com.axopen.axogeneratorapi.domain.Axomot;
import com.axopen.axogeneratorapi.service.AxomotService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/axomot")
public class AxomotController {

    private final AxomotService axomotService;

    public AxomotController(AxomotService axomotService) {
        this.axomotService = axomotService;
    }

    @PostMapping
    public ResponseEntity<Axomot> createAxomot(@RequestBody Axomot axomot) {
        return ResponseEntity
                .ok()
                .body(axomotService.addAxomot(axomot));
    }

    @GetMapping
    public ResponseEntity<List<Axomot>> getAllAxomots() {
        return ResponseEntity
                .ok()
                .body(axomotService.getAllAxomots());
    }
}
