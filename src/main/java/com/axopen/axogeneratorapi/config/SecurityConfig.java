package com.axopen.axogeneratorapi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig {

    @Configuration
    protected static class ApplicationSecurity extends WebSecurityConfigurerAdapter {

        @Value(value = "${origin.url}")
        private String originUrl;

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .csrf().disable()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .cors().configurationSource(corsConfigurationSource())
                    .and()
                    // Paramètres de sécurités
                    .authorizeRequests()
                    .anyRequest().permitAll();
        }

        public CorsConfigurationSource corsConfigurationSource() {
            CorsConfiguration configuration = new CorsConfiguration();
            configuration.setAllowedOriginPatterns(Collections.singletonList(originUrl));
            configuration.setAllowedMethods(Collections.singletonList("*"));
            configuration.setAllowedHeaders(Arrays.asList(
                    "Access-Control-Allow-Headers", "Access-Control-Allow-Origin", "Authorization",
                    "X-Requested-With", "X-Auth-Token", "Content-Type", "Accept", "X-CSRF-TOKEN", "X-Frame-Options"
            ));
            configuration.setAllowCredentials(true);
            configuration.setMaxAge(3600L);
            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
            source.registerCorsConfiguration("/**", configuration);
            return source;
        }
    }

    @RestController
    @RequestMapping("/api/health-check")
    protected static class HealthCheckEndpoint {

        @GetMapping
        public ResponseEntity<Void> healthCheck() {
            return ResponseEntity
                    .ok()
                    .build();
        }
    }
}
