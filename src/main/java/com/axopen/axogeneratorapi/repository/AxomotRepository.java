package com.axopen.axogeneratorapi.repository;

import com.axopen.axogeneratorapi.domain.Axomot;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AxomotRepository extends JpaRepository<Axomot, String> {

}
